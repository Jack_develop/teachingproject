﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;

namespace TeachingProject
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //連線測試 事件
        private void connectTestBtn_Click(object sender, EventArgs e)
        {
            SqlClass.SqlConnect();
            MessageBox.Show("成功");
        }

        //撈取資料 事件
        private void searchTestBtn_Click(object sender, EventArgs e)
        {
            //設定連結
            SqlConnection connect = SqlClass.SqlConnect();
            //資料接收位置
            DataTable data = new DataTable();
            //存取資料庫資料(會自動open close)
            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            //sql 語句，使用stringBuilder存放，可分列append 提高可閱讀性
            StringBuilder sql = new StringBuilder();
            //寫入語句
            //sql.Append("SELECT * FROM Test");
            //加入篩選條件，使用Parameters 避免sql injection，及明碼值
            if (textBox.Text.Length > 0)
            {
                sql.Append("SELECT * FROM Test where Num = @tt");
                dataAdapter = new SqlDataAdapter(sql.ToString(), connect);
                dataAdapter.SelectCommand.Parameters.Clear();
                dataAdapter.SelectCommand.Parameters.AddWithValue("@tt", textBox.Text);
            }
            else
            {
                //show訊息
                MessageBox.Show("請填入值");
            }
            //取得放置資料
            dataAdapter.Fill(data);
            //與表格元件上顯示
            this.dataGridView.DataSource = data;
        }
    }
}
