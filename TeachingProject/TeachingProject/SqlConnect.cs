﻿using System;
using System.Data.SqlClient;
using System.Text;

namespace TeachingProject
{
    class SqlClass
    {
        //設定一個公用靜態(可讓人使用) function 叫  SqlConnect 回傳屬性為SqlConnection
        public static SqlConnection SqlConnect()
        {
            //設定連線字串
            StringBuilder connectStr = new StringBuilder();
            //server
            connectStr.Append("Server=localhost\\sqlexpress;");
            //user
            connectStr.Append("user id='TestUser';");
            //password
            connectStr.Append("password='123456';");
            //database
            connectStr.Append("initial catalog=Test;");
            //連線Timeout
            connectStr.Append("Connect Timeout=30; ");

            //宣告SqlConnection 並帶入剛剛的連線設定
            SqlConnection conn = new SqlConnection(connectStr.ToString());
            
            //放在此處可讓程式發生錯誤時，做出設定好相應的動作
            try
            {
                //測試連線
                conn.Open();
                conn.Close();
            }
            //若錯誤則終止
            catch (Exception)
            {
                Environment.Exit(Environment.ExitCode);
            }

            //返回SqlConnection
            return conn;
        }
    }
}
